$(function () {
  // 天气功能


  //跨域：
  // 1.域名或ip不一样
  //127.0.0.1
  // 2.端口不一样
  //:5500
  // 3.协议不一样
  //file://
  //http://
  //通过ajax技术（异步请求数据），获取天气信息
  $.ajax({
    'url': 'http://api.map.baidu.com/telematics/v3/weather?location=广州&output=json&ak=ZbeFXv8desxK2Rmq76o5ytCk',
    'type': 'get',
    'dataType': 'jsonp', //加入这行就可以解决跨域，数据请求类型
    'success': function (data) {
      if (sessionStorage.getItem(weather)) {
        var str3 = JSON.parse(sessionStorage.getItem(weather));
        $(".weather").append(str3);
        console.log(5545);
      }
      else if (data.status == 'success') {
        var weather_data = data.results[0].weather_data[0];
        var city = data.results[0].currentCity;
        var date = weather_data.date;
        var dayPictureUrl = weather_data.dayPictureUrl;
        var nightPictureUrl = weather_data.nightPictureUrl;
        // var picturl=
        var weather = weather_data.weather;
        var wind = weather_data.wind;
        var temperature = weather_data.temperature;
        var str = `
        <div class="weather_content">
          <div class="left">
            <img src="${dayPictureUrl}" alt="">
          </div>
          <div class="middle">
            <p>${date}</p>
          </div>
          <div class="right">
            <p>${city}</p>
            <p>${weather}</p>
            <p>${wind}</p>
            <p>${temperature}</p>
          </div>
        </div>
        `;
        $(".weather").append(str);
        //存储起来
        var str2 = JSON.stringify(str);
        sessionStorage.setItem("weather", str2);
      }
    }
  })
  // personnal切换夜间模式
  $(".bar .dark").click(function () {
    $("*").css("color", "#fff");
    $("body,.header,.mynav,.bar ul li,.middlenav,footer ul").css({
      "background": "#000"
    })
  })
  $(".bar .sun").click(function () {
    $("*").css("color", "#000");
    $("body,.mynav,.bar ul li,.middlenav,footer ul").css({
      "background": "#fff"
    })
    $(".header").css({
      "background": "#ff5001",
      "color": "#fff"
    })
    $(".header p").css({
      "color": "#fff"
    })

  })
})