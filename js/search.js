$(function () {
  //节阀流，用于控制左右两个功能
  var flag = true;
  //左边导航栏跟着右边的内容动
  $(window).scroll(function () {
    //若flag为true开始这个功能
    if (flag) {
      $(".category_right .item").each(function (index, value) {
        if ($(window).scrollTop() >= $(value).offset().top) {
          $(".category_left ul li").eq(index).addClass("active").siblings().removeClass("active");
        }
      })
    }
  })
  //点击左边导航，右边内容跟着动
  $(".category_left ul li").click(function () {
    //点击后关闭 左边导航栏跟着右边的内容动 这个功能
    flag = false;
    $(this).addClass("active").siblings().removeClass("active");
    var top = $(".category_right .item").eq($(this).index()).offset().top;
    $("html,body").animate({
      scrollTop: top
    }, function () {
      //动画完毕后打开上面的功能
      flag = true;
    })
  })

})